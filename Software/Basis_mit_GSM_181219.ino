
#include <RH_RF95.h>
#include <SPI.h>
#include <String.h>
#include "TimerOne.h"
#include "SIM800L.h"

#include <LowPower.h>
#include <avr/sleep.h>              // Sleep-Modus-Lib
#include <avr/power.h>
#include <avr/wdt.h>
#define test

/****************** PIN-Belegung ********************/

#define LED 8
#define DIP1 A3
#define DIP2 A2
#define RFMRST A1
#define RFMINT 2    // Interrupt-Pin des RFM --> wird bei empfangener Nachricht ausgelöst

/***************** LORA-DEFINES *********************/

#define CLIENT_ADDRESS 102       //0-255 
#define SERVER_ADDRESS 222      //0-255
#define ID_SIZE 10            // Anzahl der möglichen IDs: Informationen speichern pro ID?

/***********ALARM-Types**************/

#define ACK 0x11
#define ALARM 0x22
#define NEWDEV 0x33     // New Device


/************LORA-Buffer und -Variablen**************/

uint8_t data[] = { 0x00, 0x00, 0x00, 0x00, 0x00 }; // Initialisierung des Data-Arrays
uint8_t IDs[ID_SIZE];
uint8_t lastID = 0;

/******************GSM-Variablen********************/

char* phonenumber = "\"+4915151644765\"";
uint8_t sens = 100; 

/*****************Timer-Variablen******************/

boolean led_EN = false;
uint16_t time1 = 1500;
uint16_t countLimit = 3000;
uint16_t counter = 0;

/*****************Sonstige-Variablen******************/
boolean block = false;

/***********Initialisierung GSM und RFM95************/

RH_RF95 rf95;
SIM800L gsm(4, 5, 9, 6);

void setup()
{
  pinConfig();
  RFMInit();
  gsm.pinInit();
  gsm.wakeUp();
  Timer1.initialize(50);
  Timer1.attachInterrupt(timer);
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  delay(100);
  Serial.begin(9600);
  delay(100);
  Serial.println("SERVER START");
  delay(100);
  led_EN = true;
 
  if (!readDIP(1)) {

    gsm.resetAndInitSMS();
    Timer1.stop();
    digitalWrite(LED, HIGH);
    delay(100);
    do {
      Serial.print("....");  delay(500);
    } while (!gsm.isConnected());
    digitalWrite(LED, HIGH);
    Serial.println("GSM Connected");
    delay(1500);
    digitalWrite(LED, LOW);

    gsm.setSMSMode(true, true, true, true, true);
    Serial.println("GSM READY");

  }
  else {
    Serial.println("GSM OFF - Lora-Ready!");
    Timer1.stop();
  }

  led_EN = false;
  //phonenumber = gsm.getPhoneBookEntry(1);
  while (!rf95.sleep()) {} // Sleep des RF-Moduls
  delay(200);
  sleepUntilMessage();

}


void loop()
{
  checkLoraMessage();
  if (gsm.isNewSMS()) {         // Wenn eine neue SMS ankommt wird diese auf das Codewort "Sens" überprüft
    Serial.println("NEW SMS");
    if (gsm.wakeUp()) {
      uint16_t tmpSens;
      tmpSens = gsm.getSMSData("Sens:");
      if (tmpSens != 0) {
        Serial.println("New Sensitivity");
        if(tmpSens > 255) sens = 255;
        else if (tmpSens <0) sens = 0;
        else sens = tmpSens; 
        if (gsm.deleteSMSMemory()) Serial.println("SMS geloescht");
      }
    }
  }

  gsm.Sleep();
  sleepUntilMessage();
}

void checkLoraMessage() {

  if (rf95.available()) // Warte auf Nachricht
  {
    uint8_t buf[50];
    uint8_t len = sizeof(buf);

    if (rf95.recv(buf, &len))
    {
      data[0] = buf[1];           // Dest-ID = Client ID
      data[1] = SERVER_ADDRESS;    // Source ID = Server Adress
      data[2] = ACK;              // Alarm-Type = Ack
      data[3] = sens;
      data[4] = buf[4];
      rf95.send(data, sizeof(data));      //Antwort senden
      rf95.waitPacketSent();
      digitalWrite(LED, HIGH);          // LED blinken lassen
      delay(500);
      digitalWrite(LED, LOW);
      while (!gsm.wakeUp()) {}
      decodeMessage(buf[1], buf[2], buf[3], buf[4]);   // Empfangene Nachricht dekodieren.
    } else  Serial.println("recv failed");
    gsm.Sleep();
  }
}

void sleepUntilMessage() {      // Sleep des Arduinos, RFM in RX-Mode, Aufwachen durch Interrupt GSM oder RFM
  
  rf95.setModeRx();
  cli();
  sleep_enable();
  sei();
  sleep_cpu();

  sleep_disable();
  delay(100);
}
void decodeMessage(uint8_t id, uint8_t alarm, uint8_t battlow, uint8_t batthigh)
{
  char tmp[40];
  switch (alarm)    // Aktuell nicht sicher in welcher Reihenfolge die Daten reinkommen: Eventuell deswegen Wrong Data
  {
    case NEWDEV: if (!isID(id))
        sprintf(tmp, "New Device Nr. %x", id);// Neues Gerät wird nur angemeldet wenn es noch nicht in der Liste ist
      break;
    case ALARM:
      sprintf(tmp, "Alarm! Device: %x ", id);
      break;
    default:    Serial.println("Wrong Data");
      break;
  }

  if (!readDIP(1)) {
    Serial.println(gsm.messageConstructor(tmp, ((batthigh << 8) | battlow), rf95.lastRssi()));
  }
  if (!readDIP(2)) { // Senden einer SMS wenn DIP geschaltet
    gsm.sendTextMessage(gsm.messageConstructor(tmp, (batthigh << 8) | battlow, rf95.lastRssi()), phonenumber);
  }
}
void timer() {

  counter = (counter + 1) % countLimit;

  if (led_EN) {
    if (counter % time1 == 0)digitalWrite(LED, digitalRead(LED) ^ 1);   // Toggeln der LED
  } else digitalWrite(LED, LOW);

}
boolean isID(uint8_t newID) { // Überprüfung ob die empfangene ID bereits in der Liste ist

  boolean found = false;
  for (int k = 0; k < ID_SIZE ; k++) { // Durchgehen aller vorhandenen IDs
    if (newID == IDs[k]) found = true;
  }
  if (found)return true;
  else {
    if (lastID >= ID_SIZE - 1) {
      Serial.println("Too many IDs"); // Wenn die Liste voll ist, ist die maximale Anzahl an Geräten pro Basis erreicht: Eventuell Meldung
      return false; 
    }
    else {
      IDs[lastID++] = newID;
      return false;
    }
  }
}
boolean readDIP(uint8_t pin) {
  switch (pin) {
    case 1: return digitalRead(DIP1);
      break;
    case 2: return digitalRead(DIP2);
      break;
    default: return false;
      break;
  }
}
void RFMInit() {
  while (!Serial) ; // Wait for serial port to be available
  if (!rf95.init()) Serial.println("init failed");
  rf95.setFrequency(868.0);
  rf95.setTxPower(23, false);
  rf95.setPayloadCRC(true);
}
void pinConfig() {

  pinMode(RFMINT, INPUT);
  pinMode(LED, OUTPUT);
  pinMode(DIP1, INPUT_PULLUP);
  pinMode(DIP2, INPUT_PULLUP);
  pinMode(RFMRST, OUTPUT);
  digitalWrite(RFMRST, LOW);

}
