#include "SIM800L.h"
#include <SoftwareSerial.h>

SIM800L::SIM800L(uint8_t RX, uint8_t TX, uint8_t DTR, uint8_t RST): sim800l(RX, TX) { //Klassen-Constructor --> Übergabe der Pinnummern und Starten der seriellen Verbindung zum GSM-Modul

  this->RX = RX;
  this->TX = TX;
  this->DTR = DTR;
  this->RST = RST;
  sim800l.begin(9600);

}

/*********Initialsierungen**************/

void SIM800L::pinInit() {      // Initialsieren der Pins DTR und RST, Active low

  pinMode(RST, OUTPUT);
  pinMode(DTR, OUTPUT);
  digitalWrite(RST, HIGH);
  digitalWrite(DTR, HIGH);

}

void SIM800L::setSMSMode(boolean loraBatt, boolean GSMBatt, boolean loraRSSI, boolean GSMRSSI, boolean credit) {
// kopieren der Werte in lokales Array
  this->mode[0] = loraBatt;
  this->mode[1] = GSMBatt;
  this->mode[2] = loraRSSI;
  this->mode[3] = GSMRSSI;
  this->mode[4] = credit;

}

void SIM800L::resetAndInitSMS() {     // Netzeinwahl und Umstellen auf SMS-Modus

  digitalWrite(RST, LOW);     // Kurzer Reset (Sicherheit)
  delay(150);
  digitalWrite(RST, HIGH);
  delay(3000);
  serialFlush();              // leeren des Seriellen Buffers um Fehlerhafte Daten zu löschen
     sim800l.println("AT+CPIN=0000\r");            // Eingabe des SIM-Karten Pins ==> Hier 0000
  delay(200);
  sim800l.println("AT+CMGF=1\r"); // Set the shield to SMS mode
  delay(200);
  sim800l.println("AT+CNMI=1,1,0,0,0\r");         // Speichermodus ankommender SMS --> werden im Speicher abgelegt und können damit bei Bedarf ausgelesen werden
  delay(200);

}
/**************Funktionen für die serielle Kommunikation mit dem SIM800L******************/

String SIM800L::messageConstructor(char* message, uint16_t loraBatt, int16_t RSSILora) {      // Zusammensetzen der SMS an den Jäger/Nutzer

  char sumbuff[150] = { "" };      // leeres Buffer für die Summe
  char buff2[70];                   // leeres Buffer für die Einzelwerte
  strcat(sumbuff, message);

  if (mode[0]) {                    // Je nach Auswahl werden unterschiedliche Informationen an die SMS angehängt:
    sprintf(buff2, "\nClient-Batt: %d mV ", loraBatt);          // Client-Batteriespannung
    strcat(sumbuff, buff2);
  }
  delay(100);
  if (mode[1]) {                                                // Basis-Batteriespannung
    uint16_t m = getBatteryVoltage();
    sprintf(buff2, "\nBasis-Batt: %d mV ", m);
    strcat(sumbuff, buff2);
  }
  if (mode[2]) {                                              // Lora-Verbindungsqualität (RSSI)
    sprintf(buff2, "\nClient-QoS: %d ", RSSILora);
    strcat(sumbuff, buff2);
  }
  if (mode[3]) {                                              // GSM-Verbindungsqualität (nicht RSSI, Umrechnung erforderlich)
    int conn = getConnectionQuality();
    sprintf(buff2, "\nBasis-QoS: %d ", conn);
    strcat(sumbuff, buff2);
  }
  if (mode[4]) {                                            // Prepaid Guthaben in Euro
    strcat(sumbuff, "\nGuthaben: ");
    dtostrf(getPrepaidCredit(), 4, 2, buff2);
    strcat(sumbuff, buff2);
  }

  delay(10);
  return sumbuff;

}
// Liest immer bis zum Ende eines Befehls (\r) aus dem Seriellen Monitor und setzt dann newData auf true
void SIM800L::recvWithEndMarker() {     
 
  static byte ndx = 0;
  char endMarker = '\r';
  char rc;

  while (sim800l.available() > 0 && newData == false) { // Wenn neue Daten anliegen werden diese eingelesen
    rc = sim800l.read();

    if (rc != endMarker) {    //einlesen, solange kein \r vorliegt
      receivedChars[ndx] = rc;
      ndx++;
      if (ndx >= numChars) {
        ndx = numChars - 1;
      }
    }
    else {      
      receivedChars[ndx] = '\0'; // String abschließen und mit NewData neue Daten anzeigen
      ndx = 0;
      newData = true;

    }
  }
}

boolean SIM800L::isNewSMS() {
  recvWithEndMarker();                     // Gibt so lange false zurück bis CMTI gefunden wurde
  if (newData == true) {
    newData = false;
    if (strstr(receivedChars, "CMTI:"))              // Erneute Überprüfung ob gewünschtes Wort
      return true;
    else return false;
  }
  else return false;

}

char* SIM800L::searchSerialResponse(char* x, long timeout) {    
  // Durchsucht die serielle Schnittstelle nach einem Codewort. 
  // Dafür ruft es recvWithEndMarker() mehrfach auf bis das Wort gefunden oder ein Timeout gesehenen ist

  long duration = 0;

  do {
    do {
      recvWithEndMarker();
      duration++;
      if (duration > timeout * 50) break;
    } while (!newData);       // Solange kein vollständiges Datenwort gefunden wurde  --> Weiterlesen
    newData = false;

    if (duration > timeout * 50) break;
  } while (!strstr(receivedChars, x));        // --> So lange das gefundene Wort nicht das gesuchte ist 
                                                //--> Wort verwerfen und weitersuchen
  if (strstr(receivedChars, x)) {             // Erneute Überprüfung ob gewünschtes Wort
    newData = false;
    return strstr(receivedChars, x);            // Rückgabe des Pointers auf die Startadresse des gesuchten Wortes
  }
  else {
    newData = false;
    Serial.println("Timeout");                // Rückgabe von einem NULL Zeiger bei einem Timeout
    return NULL;
  }
}

int SIM800L::getIntFromString(char *stringWithInt, byte num)      // Zeiger auf die Startadresse des Strings, num = x-ter Integer Wert im String
{
  char *tail;
  while (num > 0)
  {
    num--;
    // skip non-digits
    while ((!isdigit(*stringWithInt)) && (*stringWithInt != 0)) stringWithInt++;
    tail = stringWithInt;
    // find digits
    while ((isdigit(*tail)) && (*tail != 0)) tail++;
    if (num > 0) stringWithInt = tail; // new search string is the string after that number
  }
  return (strtol(stringWithInt, &tail, 0));
}


/**********Hilfsfunktionen Seriell********************/

void SIM800L::serialFlush() {   //liest alles verfügbaren Buffer-Werte aus und überschreibt sie

  while (sim800l.available() > 0) {
    Serial.write(sim800l.read());
  }
}

void SIM800L::updateSerial() {  //für Debug-Zwecke

  while (Serial.available()) {
    sim800l.write(Serial.read());// Weitergabe der USB-Daten an das GSM-Modul
  }
  while (sim800l.available()) {
    Serial.write(sim800l.read());//Weitergabe der GSM-Daten an HW-Serial
  }
}

/*******************************GSM-Funktionen***********************************/

int SIM800L::getSMSData(char* repl) {       // Gibt den Integer-Wert zurück der hinter der gesuchten Antwort (repl) steht z.B. Sens: 123 gibt 123 zurück
  sim800l.print("AT+CMGL\r");               // ruft die letzte ungelesene SMS aus dem Speicher auf
  char* pos = searchSerialResponse(repl, 6000);       //Darin wird nach dem Codewort gesucht, bei einem Timeout wird 0 zurückggeben--> ungültig
  if (pos != NULL) return getIntFromString(pos, 1);
  else return 0;
}
boolean SIM800L::sendTextMessage(String message, char* num) // message wird an angegebene Telefonnummer geschickt, gibt true zurück wenn das Senden erfolgreich war.
{
  sim800l.print("AT+CMGF=1\r"); // Set the shield to SMS mode
  delay(100);
  sim800l.print("AT+CMGS=");
  sim800l.print(num);
  sim800l.print("\r");
  delay(100);
  sim800l.print(message);
  sim800l.print("\r"); //the content of the message
  delay(100);
  if (searchSerialResponse("CMGS:", 5000) != NULL) {
    return true;
  }
  else return false;
}


boolean SIM800L::Sleep() {
  //AT + CSCLK = 1
  sim800l.print("AT+CSCLK=1\r");
  char* pos = searchSerialResponse("OK", 6000);
  if (pos != NULL) {
    Serial.println("Sleep");
    return true;
  }
  else return false;
}

boolean SIM800L::PowerDown() {
  // AT+CPOWD=1
  // Aufwecken ueber RST (eher Notloesung)
  sim800l.print("AT+CPOWD=1\r");
  char* pos = searchSerialResponse("NORMAL POWER DOWN", 6000);
  if (pos != NULL) return true;
  else return false;
}
boolean SIM800L::wakeUp() {
  // Alternativ SMS oder Anruf --> Wakeup
  digitalWrite(DTR, LOW);
  delay(700);
  sim800l.print("AT+CSCLK=0\r");
  char* pos = searchSerialResponse("OK", 6000);
  if (pos != NULL) {
    digitalWrite(DTR, HIGH);
    Serial.println("WakeUp");
    delay(300);
    return true;
  }
  else return false;
}


boolean SIM800L::deleteSMSMemory() {

  //AT + CMGD = "DEL ALL" --> l�scht alle
  //Antwort = OK/Error oder +CMS eventuell lange Response time bei vielen NAchrichten
  // Alternativ AT+CMGDA l�scht alle
  sim800l.print("AT+CMGDA=\"DEL ALL\"\r");
  char* pos = searchSerialResponse("OK", 6000);
  if (pos != NULL) return true;
  else return false;
}

int SIM800L::getTemp() {
  //AT+CMTE?
  sim800l.print("AT+CMTE?\r");
  char* pos = searchSerialResponse("CMTE:", 3000);
  if (pos != NULL) return getIntFromString(pos, 2);    // Wenn kein Timeout aufgetreten ist wird der zweite Integerwert zurückgegeben
  else return 0;
}

int SIM800L::getLocalTime() {
  // AT+CCLK
  //Antwort: +CLTS: "yy/MM/dd,hh:mm:ss+/-zz"
  // Funktionierte im Test nicht, da nicht jeder Provider die Zeit übermittelt
  // Eventuell spätere Umsetzung
}
float SIM800L::getPrepaidCredit() {   // Sendet +100# und wartet auf Antwort mit "Guthaben"

  sim800l.print("AT+CUSD=1,\"*100#\"\r");
  char* pos = searchSerialResponse("Guthaben", 6000);
  if (pos != NULL) return (getIntFromString(pos, 1) * 1.0 + getIntFromString(pos, 2) * 0.01);    // Wenn kein Timeout aufgetreten ist wird aus 2 Integerwerten das Guthaben zu einem Float zusammengesetzt;
  else return 0.0;

}


boolean SIM800L::isConnected() {          // Bei Antwort +CREG 0,1 ist GSM Modul eingewählt

  sim800l.print("AT+CREG?\r");
  char* pos = searchSerialResponse("CREG:", 5000);
  if (pos != NULL && getIntFromString(pos, 1) == 0 && getIntFromString(pos, 2) == 1)return true;
  else return false;
}

// Ähnliches Vorgehen wie in obigen Funktionen:

int SIM800L::getConnectionQuality() {

  sim800l.print("AT+CSQ\r");
  char* pos = searchSerialResponse("CSQ:", 2000);
  if (pos != NULL) return getIntFromString(pos, 1);
  else return 0;
}


int SIM800L::getBatteryPercentage() {

  sim800l.print("AT+CBC\r");
  char* pos = searchSerialResponse("CBC:", 2000);
  if (pos != NULL) return getIntFromString(pos, 2);
  else return 0;
}

int SIM800L::getBatteryVoltage() {

  sim800l.print("AT+CBC\r");
  char* pos = searchSerialResponse("+CBC:", 5000);
  if (pos != NULL) return getIntFromString(pos, 3);
  else return 0;
}

char* SIM800L::getPhoneBookEntry(int num) {

  sim800l.print("AT+CPBR=");
  sim800l.print(num);
  sim800l.print("\r");
  char* pos = searchSerialResponse("CPBR:", 2000);
  if (pos != NULL) {
    char* ptr = strtok(receivedChars, ",");
    ptr = strtok(NULL, ",");
    return ptr;
  }
  else return NULL;
}
