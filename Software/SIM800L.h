#ifndef __GSM_SIM800L__
#define __GSM_SIM800L__
#define numChars 60
#include <Arduino.h>
#include <SoftwareSerial.h>

class SIM800L {
  private:

    //Pinbelegung
    uint8_t RX;
    uint8_t TX;
    uint8_t DTR;
    uint8_t RST;

    //GSM-Modus-Array
    boolean mode[5] = { true, false, false, false, false };
    boolean newData = false;
    char receivedChars[numChars];   // an array to store the received data

    //Private Funktionen, die nur von der Bibliothek verwendet werden
    void recvWithEndMarker();
    char* searchSerialResponse(char* x, long timeout);
    int getIntFromString(char *stringWithInt, byte num);
    void serialFlush();


    //Initialisierung Software Serial
    SoftwareSerial sim800l; // RX, Tx

  public:

    //Constructor:
    SIM800L(uint8_t RX, uint8_t TX, uint8_t DTR, uint8_t RST);

    //Initialisierungen:
    void setSMSMode(boolean loraBatt, boolean GSMBatt, boolean loraRSSI, boolean GSMRSSI, boolean credit);
    void resetAndInitSMS();
    void pinInit();
    String messageConstructor(char* message, uint16_t loraBatt, int16_t RSSILora);
    boolean isNewSMS();

    // Funktionen GSM:
    boolean sendTextMessage(String message, char* num);
    int getSMSData(char* repl);
    void updateSerial();
    boolean Sleep();
    boolean PowerDown();
    boolean wakeUp();
    boolean deleteSMSMemory();
    int getTemp();
    int getLocalTime();
    float getPrepaidCredit();
    boolean isConnected();
    int getConnectionQuality();
    int getBatteryPercentage();
    int getBatteryVoltage();
    char* getPhoneBookEntry(int num);

};

#endif
